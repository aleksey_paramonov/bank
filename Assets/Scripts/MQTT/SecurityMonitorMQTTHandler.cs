﻿using UnityEngine;
using System.Collections;
using MqttLib;
using SimpleJSON;

public class SecurityMonitorMQTTHandler : MQTTHandlerBase
{
	override protected string clientId { get { return "SecurityMonitor"; } }

	public Transform point;

	protected const float SMOOTH_TIME_ROTATION = 3.0f;
	protected const float SMOOTH_TIME_POSITION = 0.14f;
	protected Vector3 curVelocity;
	protected Vector3 targetPosition;
	protected Quaternion targetRotation;

	override public void Awake()
	{
		base.Awake();
		targetPosition = Vector3.zero;
		targetRotation = Quaternion.identity;
		curVelocity = Vector3.one;
	}

	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	override public void Update()
	{
		base.Update();
//		point.transform.position = Vector3.SmoothDamp(point.transform.position, targetPosition, ref curVelocity, SMOOTH_TIME_POSITION);
		//point.transform.rotation = Quaternion.Slerp( point.transform.rotation, targetRotation, SMOOTH_TIME_ROTATION * Time.deltaTime );
		point.transform.position = targetPosition;
	}

	override protected void OnConnect(object sender, System.EventArgs e)
	{
		base.OnConnect(sender, e);
		client.Subscribe(Config.kCarPositionTopic, QoS.BestEfforts);
	}

	override protected bool OnMessageArrived(object sender, PublishArrivedArgs e)
	{
		base.OnMessageArrived(sender, e);

		JSONNode data = JSON.Parse(e.Payload.ToString());

		JSONArray pos = data["pos"].AsArray;
		targetPosition.x = pos[0].AsFloat;
		targetPosition.y = 2f;
		targetPosition.z = pos[2].AsFloat;

		return true;
	}
}
