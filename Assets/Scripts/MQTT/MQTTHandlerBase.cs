﻿using UnityEngine;
using System.Collections;
using MqttLib;
using SimpleJSON;

public class MQTTHandlerBase : MonoBehaviour
{

	protected IMqtt client;

	protected virtual string clientId { get { return "NOT_SET"; } }

	void SendAliveMessage(){
	
		try{
			client.Publish("android/alive/state","Alive", QoS.OnceAndOnceOnly, false);
		}
		catch{
			client.Disconnect();
		}
		
	}

	public virtual void Awake()
	{
		client = MqttClientFactory.CreateClient(Config.kConnectionString, clientId);
		client.Connected += new ConnectionDelegate(OnConnect);
		client.ConnectionLost += new ConnectionDelegate(OnConnectionLost);
		client.PublishArrived += new PublishArrivedDelegate(OnMessageArrived);
		Connect();
		InvokeRepeating("SendAliveMessage", 5,5);
	}

	protected void Connect()
	{
		try {
			client.Connect(true);
		} catch (System.Exception ex) {
			Debug.LogError("Failed to connect to " + Config.kConnectionString);
			Debug.LogException(ex);
		}
	}


	public virtual void Update()
	{

		if (!client.IsConnected) {
			Connect();
		}
	}

	void OnDisable()
	{
		client.Disconnect();
	}

	#region Callbacks

	protected virtual void OnConnect(object sender, System.EventArgs e)
	{
		Debug.Log(clientId + " connected");
	}

	protected virtual void OnConnectionLost(object sender, System.EventArgs e)
	{
		Debug.LogError(clientId + " connection lost");
	}

	protected virtual bool OnMessageArrived(object sender, PublishArrivedArgs e)
	{
		Debug.Log(clientId + " received message: " + e.Payload.ToString());

		return true;
	}

	#endregion
}
