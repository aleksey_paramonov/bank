﻿using UnityEngine;
using System.Collections;
using MqttLib;
using System;
using SimpleJSON;
using UnityEngine.UI;

public class CarMQTTHandler : MQTTHandlerBase
{
	override protected string clientId { get { return "Car"; } }

	// Объект машинки на карте
	public Transform car;
	public CarController carController;

	// Точка начала движения
	public Transform startPoint;

	public GameObject StartGame;
	public GameObject PauseGame;

	public Image rssiGUI;
	// интервал передачи данных в милисекундах
	protected const float TIME_TO_SEND_DATA_MAX = 1f;

	// текущее время до отправки данных
	protected float timeToSendData = 0;

	// Флаг выставляется при получении команды сброса положения машинки
	public enum LabState{shouldResetPosition, shouldPause, shouldResume, None};
	LabState state=LabState.None;
	bool isRecieved=false;
	int gSignalStrength=0;
	int kNoSignalStrength = (-12);
	int kPoorSignalStrength = (-14);
	int kMiddleSignalStrength = (-16);
	int kHighSignalStrength = (-19);
	
	#region Unity

	// Update is called once per frame
	override public void Update()
	{
		base.Update();

		ShowSignalStrength();

		switch(state)
		{
			case LabState.shouldResetPosition:
				ResetCarPosition();
				state=LabState.None;
				break;

			case LabState.shouldPause:
				PauseGame.gameObject.SetActive(true);
				StartGame.gameObject.SetActive(false);
				state=LabState.None;
				break;
			
			case LabState.shouldResume: 
				PauseGame.gameObject.SetActive(false);
				StartGame.gameObject.SetActive(true);
				state=LabState.None;
				break;
			
			default:
				break;
		}

		if (client.IsConnected) {
			timeToSendData += Time.deltaTime;

			if (timeToSendData >= TIME_TO_SEND_DATA_MAX) {
				timeToSendData = 0;
				SendCarMessage();
			}
		}
	}

	#endregion
	void ShowSignalStrength(){
		//Debug.Log ("RSSI: "+gSignalStrength.ToString ());
		if(gSignalStrength>=kNoSignalStrength)
			rssiGUI.sprite = Resources.Load<Sprite>("no-signal") as Sprite;
		else if(gSignalStrength<=kPoorSignalStrength && gSignalStrength>kMiddleSignalStrength)
			rssiGUI.sprite = Resources.Load<Sprite>("poor") as Sprite;
		else if(gSignalStrength<=kMiddleSignalStrength && gSignalStrength>kHighSignalStrength)
			rssiGUI.sprite = Resources.Load<Sprite>("middle") as Sprite;
		else if(gSignalStrength<=kHighSignalStrength)
			rssiGUI.sprite = Resources.Load<Sprite>("strongest") as Sprite;
	}
	#region Manage connection

	// отправка данных о машинке
	void SendCarMessage()
	{
		JSONClass data = new JSONClass();

		// позиция машинки
		{
			JSONArray pos = new JSONArray();
			pos[0] = car.transform.position.x.ToString();
			pos[1] = car.transform.position.y.ToString();
			pos[2] = car.transform.position.z.ToString();
			data.Add("pos", pos);
		}

		// поворот машинки
		{
			JSONArray rot = new JSONArray();
			rot[0] = car.transform.rotation.x.ToString();
			rot[1] = car.transform.rotation.y.ToString();
			rot[2] = car.transform.rotation.z.ToString();
			rot[3] = car.transform.rotation.w.ToString();
			data.Add("rot", rot);
		}

		// время
		{
			data.Add("time", Time.realtimeSinceStartup.ToString());
		}
		try {
			client.Publish(Config.kCarPositionTopic, data.ToString(), QoS.OnceAndOnceOnly, false);	
		} catch (Exception ex) {
			client.Disconnect();
		}

	}

	#endregion

	public void TestSend(){
		try {
			if(isRecieved==false)
				client.Publish("bank/Labirinth/state", "finished", QoS.OnceAndOnceOnly, false);	
		} catch (Exception ex) {
			client.Disconnect();
		}
	}

	#region MQTTHandlerBase overrides

	// событие успешного подключения
	override protected void OnConnect(object sender, EventArgs e)
	{
		base.OnConnect(sender, e);
		client.Subscribe(Config.kLabirinthCommandsTopic, QoS.BestEfforts);
	}

	// событие получение сообщения
	override protected bool OnMessageArrived(object sender, PublishArrivedArgs e)
	{
		base.OnMessageArrived(sender, e);
		bool result = base.OnMessageArrived(sender, e);
		string message = e.Payload.ToString();

		if (message == Config.kPauseCommand) {
			state=LabState.shouldPause;
		}
		if (message == Config.kResumeCommand) {
			state=LabState.shouldResume;
		}
	
		if (message == Config.kReceiveCommand) {
			isRecieved=true;
		}
		if (message == Config.kResetCommand) {
				state = LabState.shouldResetPosition;
				isRecieved=false;

		} else {
			gSignalStrength = int.Parse(message);

		}


		return result;
	}

	#endregion

	#region Helper methods

	// сброс положения машинки в начальную точку
	void ResetCarPosition()
	{
		carController.Reset();
		car.transform.position = startPoint.transform.position;
		car.transform.rotation = startPoint.transform.rotation;
	}


	#endregion
}







