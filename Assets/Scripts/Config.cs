﻿using System;

public static class Config
{
	// параметры доступа к серверу
	//public const string kConnectionString = "tcp://iot.eclipse.org:1883";
	public const string kConnectionString = "tcp://bank.server.local:1883";
	//public const string kConnectionString = "tcp://10.10.50.176:1883";
	
	public const string kCarPositionTopic = "bank/car/position";
	//public const string kAliveTopic = "android/alive/state";
	public const string kLabirinthCommandsTopic = "bank/Labirinth/commands";
	public const string kResetCommand = "reset";
	public const string kPauseCommand = "pause";
	public const string kReceiveCommand = "received";
	
	public const string kResumeCommand = "resume";
}

