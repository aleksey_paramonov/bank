﻿using UnityEngine;
using System.Collections;

public class StandaloneControl : MonoBehaviour {

    #if !MOBILE_INPUT
	void Update()
    {
        CrossPlatformInput.SetAxis("Horizontal", Input.GetAxisRaw("Horizontal"));
        CrossPlatformInput.SetAxis("Vertical"  , Input.GetAxisRaw("Vertical"  ));
	}
    #endif
}
