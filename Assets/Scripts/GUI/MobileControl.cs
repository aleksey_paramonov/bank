﻿using UnityEngine;
using System.Collections;

public class MobileControl : MonoBehaviour {

    public dfButton moveForwardButton;
    public dfButton moveBackwardButton;
    public dfButton turnLeftButton;
    public dfButton turnRightButton;

    private void OnDisable() {
        moveForwardButton.gameObject.SetActive(false);
        moveBackwardButton.gameObject.SetActive(false);
        turnLeftButton.gameObject.SetActive(false);
        turnRightButton.gameObject.SetActive(false);
    }

    private void OnEnable() {
        moveForwardButton.gameObject.SetActive(true);
        moveBackwardButton.gameObject.SetActive(true);
        turnLeftButton.gameObject.SetActive(true);
        turnRightButton.gameObject.SetActive(true);
    }
}
