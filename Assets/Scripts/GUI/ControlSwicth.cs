﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MobileControl    ))]
[RequireComponent(typeof(StandaloneControl))]
public class ControlSwicth : MonoBehaviour {

    public enum Type {

        MobileControl,

        StandaloneControl,

    };

    public  Type type = Type.StandaloneControl;
	
    private MobileControl       m_mobileControl;
    private StandaloneControl   m_standaloneControl;

	private void Start() {
        #if MOBILE_INPUT
        type = Type.MobileControl;
        #endif
        m_mobileControl     = GetComponent<MobileControl    >();
        m_standaloneControl = GetComponent<StandaloneControl>();
	}

    private void Update() {
        switch (type) {
        case Type.MobileControl:
            SwitchToMobileControl();
            break;
        case Type.StandaloneControl:
            SwitchToStandaloneControl();
            break;
        }
    }

    private void SwitchToMobileControl() {
        m_standaloneControl.enabled = false;
        m_mobileControl.enabled     = true;
    }

    private void SwitchToStandaloneControl() {
        m_mobileControl.enabled     = false;
        m_standaloneControl.enabled = true;
    }
}
