﻿using UnityEngine;
using System.Collections;
using System;

public class GenericInput : VirtualInput {

    public override float GetAxis(string name, bool raw)
    {
        CrossPlatformInput.VirtualAxis axis;
        if (!virtualAxes.TryGetValue(name, out axis)) {
            return 0.0F;
        }
        return (raw) ? axis.GetValueRaw : axis.GetValue;
    }

    public override bool GetButton(string name, CrossPlatformInput.ButtonAction action)
    {
        CrossPlatformInput.VirtualButton button;
        if (!virtualButtons.TryGetValue(name, out button)) {
            return false;
        }
        switch (action) {
        case CrossPlatformInput.ButtonAction.GetButton:
            return button.GetButton;
        case CrossPlatformInput.ButtonAction.GetButtonDown:
            return button.GetButtonDown;
        case CrossPlatformInput.ButtonAction.GetButtonUp:
            return button.GetButtonUp;
        }
        return false;
    }

    public override Vector3 MousePosition()
    {
        return virtualMousePosition;
    }

    public void SetAxis(string name, float value)
    {
        CrossPlatformInput.VirtualAxis axis;
        if (!virtualAxes.TryGetValue(name, out axis)) {
            return;
        }
        axis.Update(value);
    }
}
