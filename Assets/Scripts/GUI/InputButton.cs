﻿using UnityEngine;
using System.Collections;

public class InputButton : MonoBehaviour {

    public enum Action {
        MoveForward,
        MoveBackward,
        TurnLeft,
        TurnRight,
    };
	
    public Action action;

    private void OnMouseDown() {
        switch (action) {
        case Action.MoveForward:
            CrossPlatformInput.SetAxis("Vertical"  ,  1.0F);
            break;
        case Action.MoveBackward:
            CrossPlatformInput.SetAxis("Vertical"  , -1.0F);
            break;
        case Action.TurnLeft:
            CrossPlatformInput.SetAxis("Horizontal", -1.0F);
            break;
        case Action.TurnRight:
            CrossPlatformInput.SetAxis("Horizontal",  1.0F);
            break;
        }
    }

    private void OnMouseUp() {
        switch (action) {
        case Action.MoveForward:
            CrossPlatformInput.SetAxis("Vertical"  ,  0.0F);
            break;
        case Action.MoveBackward:
            CrossPlatformInput.SetAxis("Vertical"  ,  0.0F);
            break;
        case Action.TurnLeft:
            CrossPlatformInput.SetAxis("Horizontal",  0.0F);
            break;
        case Action.TurnRight:
            CrossPlatformInput.SetAxis("Horizontal",  0.0F);
            break;
        }
    }
}
